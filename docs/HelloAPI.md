# HelloAPI

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**hello**](HelloAPI.md#hello) | **GET** /hello | GreetingService


# **hello**
```swift
    open class func hello(name: String, completion: @escaping (_ data: GreetingResponse?, _ error: Error?) -> Void)
```

GreetingService

GreetingService

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import slcpocapi

let name = "name_example" // String | 

// GreetingService
HelloAPI.hello(name: name) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String** |  | 

### Return type

[**GreetingResponse**](GreetingResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

